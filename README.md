# HOW DO WE FEEL #

# Summary: #

In this project I wanted to show, how different a month passes by for different kind of people. 
I collected their feelings in a data visualisation. 
You can here see May 2017 through the eyes of Liza and Neil, both living in Berlin. 
I looked through their blogs and devided their statements in four different feelings. 
I also added one quote to each feeling bubble.

# Project by Mascha Camino using Unity Engine