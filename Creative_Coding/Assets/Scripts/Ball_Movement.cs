﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Ball_Movement : MonoBehaviour {

	public float MinForce = 20f; 
	public float MaxForce = 40f; 
	Rigidbody2D m_rigid;
	public float DirectionChangeInterval = 1f; 

	private float directionChangeInterval; 
	public float x;
	public float y;
	Transform mytransform;

	void Start()
	{
		
	}

	void Update()
	{
		mytransform = this.transform.GetComponent<RectTransform> ();
		// Wenn die Smileys außerhalb des Canvas auf der x-Achse sind ändern sie die Richtung
		if (mytransform.localPosition.x > 462 || mytransform.localPosition.x   < -462) {
			x = -x; 
		}
		// Wenn die Smileys außerhalb des Canvas auf der x-Achse sind ändern sie die Richtung
		if (mytransform.localPosition.y > 320 || mytransform.localPosition.y < -330) {

			y = -y; 
		}

		transform.Translate (new Vector2 (x, y) * Time.deltaTime );

		/*directionChangeInterval -= Time.deltaTime; 
		if (directionChangeInterval < 0) {

			Push (); 
			directionChangeInterval = DirectionChangeInterval; 
		}*/
	}

	void FixedUpdate(){

		x = Random.Range (-100f, 100f);
		y = Random.Range (-100f, 100f);


	}
}