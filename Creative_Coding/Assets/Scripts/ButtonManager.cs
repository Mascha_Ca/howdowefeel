﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class ButtonManager : MonoBehaviour {

	public void LoadLiza(){

		SceneManager.LoadScene ("Scene_mayLiza"); 

	}

	public void BackToMenu(){

		SceneManager.LoadScene ("Start_Menue"); 
	}


	public void LoadNeil(){

		SceneManager.LoadScene ("Scene_MayNeil"); 

	}

	public void LoadIntro(){

		SceneManager.LoadScene ("Introduction"); 
	}
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
